<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 12/5/2018
 * Time: 3:52 PM
 *
 * NB, please run PHPUnit in order to run the tests
 * Please be advised the TEST cases are right at the bottom of this file
 * They are included as their own Class
 */

class Assessment
{
    /**
     * function closestToZero
     *
     * This function finds the number that are closest to zero in a given array
     * should there be a negative and positive number equally close to zero,
     * it will use the positive value
     *
     * @param array $arrayValues
     * @return int $closestValue
     */
    function closestToZero($arrayValues=array())
    {
        // Initiate the closest value as 0 in the case that the array is empty, so that we can return 0
        $closestValue = 0;

        if(!empty($arrayValues))
        {
            // If the array is not empty, set the closest value to the highest
            // value in the array so that we can work our way back
            $closestValue = max($arrayValues);
            foreach ($arrayValues as $currentNumber)
            {
                // Use the absolute function in order to easier match(positive numbers) the current number to the last closest number
                $tempClosestValue = $closestValue;
                $closestValue = abs($closestValue) < abs($currentNumber) ? $closestValue : $currentNumber;

                // If the number are equally close to zero, take the positive value
                if(abs($tempClosestValue) == abs($currentNumber))
                {
                    $closestValue = abs($tempClosestValue);
                }
            }
        }

        return $closestValue;
    }

    /**
     * function forLoop
     *
     * This function computes the sum of numbers in an array by using a for-loop
     *
     * @param array $arrayValues
     * @return int $sum
     */
    function forLoop($arrayValues=array())
    {
        $sum = 0;
        for ($i=0; $i < count($arrayValues); $i++)
        {
            // Simply add every number in the array to the base "sum" variable
            $sum += $arrayValues[$i];
        }

        return $sum;
    }

    /**
     * function whileLoop
     *
     * This function computes the sum of numbers in an array by using a while-loop
     *
     * @param array $arrayValues
     * @return int $sum
     */
    function whileLoop($arrayValues=array())
    {
        $sum = $counter = 0;
        while($counter < count($arrayValues))
        {
            // Simply add every number in the array to the base "sum" variable
            $sum += $arrayValues[$counter];

            // Keep a counter to see when we reach the limit of the number array
            $counter++;
        }

        return $sum;
    }

    /**
     * function recursiveFunction
     *
     * This function computes the sum of numbers in an array by using a recursive function
     *
     * @param array $arrayValues
     * @return int $sum
     */
    function recursiveFunction($arrayValues)
    {
        $sum = 0;

        if(is_array($arrayValues))
        {
            foreach ($arrayValues as $val)
            {
                // Add every number in the array to the base "sum" variable by sending it through the function recursively
                $sum += $this->recursiveFunction($val);
            }
        }
        else
        {
            // Add the value that came through as a parameter
            $sum = $arrayValues;
        }

        return $sum;
    }

    /**
     * function recursiveFunction
     *
     * This function merges two different arrays alternatively using a for-loop
     *
     * @param array $arrayA
     * @param array $arrayB
     * @return array $result
     */
    function combineArrays($arrayA=array(), $arrayB=array())
    {
        // Initiate empty array that will house the result of the two arrays merged together
        $result = array();

        // Inside the for loop I check for either reaching arrayA's limit OR arrayB's,
        // because I want to add all values from both arrays
        for ($i=0; ($i<count($arrayA) || $i<count($arrayB)); $i++) {

            // I first check if there is a number available for the current iteration in arrayA
            // if there is, I add it to the housing array
            if(isset($arrayA[$i]))
                $result[] = $arrayA[$i];

            // Then I check if there is a number available for the current iteration in arrayB
            // if there is, I add it to the housing array
            if(isset($arrayB[$i]))
                $result[] = $arrayB[$i];
        }

        return $result;
    }

    /**
     * function combineArraysUsingMap
     *
     * This function merges two different arrays alternatively using array_map
     *
     * @param array $arrayA
     * @param array $arrayB
     * @return array $result
     */
    function combineArraysUsingMap($arrayA=array(), $arrayB=array())
    {
        // Initiate empty array that will house the result of the two arrays merged together
        $result = array();

        // I run the array_map function in order to easily iterate over both arrays at the same time
        array_map(
            function ($valueA, $valueB) use (&$result)
            {
                // If there is a value available in arrayA for the current iteration in the map
                // it get's pushed onto the housing array
                if(!empty($valueA))
                    array_push($result, $valueA);

                // If there is a value available in arrayB for the current iteration in the map
                // it get's pushed onto the housing array
                if(!empty($valueB))
                    array_push($result, $valueB);
            },
            $arrayA,
            $arrayB);

        return $result;
    }

    /**
     * function fibonacci
     *
     * This function prints out the first 100 fibonacci numbers
     * and gives the final result of the last 2 numbers
     *
     * @param int $fibonaccis
     * @return int $fibNum1
     */
    function fibonacci($fibonaccis=100)
    {
        // Initiate some base variables
        $counter = 0;
        $fibNum1 = $fibNumsTotal = 0;

        // We set the second number to "1" from the start in order to perform the first calculation
        $fibNum2 = 1;

        // The counter will allow us to specify any amount to loop through (using the function param),
        // for the purpose of this test we set it to 100 as per the required task on the dev test
        while($counter < $fibonaccis)
        {
            // Print out the current fibonacci number
            echo $fibNum1,'<br />';

            // Add the two most recent fibonacci numbers together in order to get the next number in line
            $fibNumsTotal = $fibNum1+$fibNum2;

            // Set the first number to the second number for the next round of calculations
            $fibNum1 = $fibNum2;

            // Set the second number to the new calculated total between the 2 most recent numbers
            // to be used in the next round of calculation
            $fibNum2 = $fibNumsTotal;

            $counter++;
        }

        return $fibNum1;
    }

    /**
     * function absoluteSum
     *
     * This function finds the lowest absolute sum between elements in an array
     *
     * @param array $arrayValues
     * @return int $sum
     */
    function absoluteSum($arrayValues=array())
    {
        // Initiate a container array that will house all the possible sum results between all the available numbers
        $sums = array();

        // Loop through every available number in the number array in order to do calculations on every other number
        for($i=0; $i<count($arrayValues); $i++)
        {
            // Loop through the same number array so that we can do a sum between the current number and every other number in the array
            for($j=0; $j<count($arrayValues); $j++)
            {
                // If the current loop iteration is the same as the number we're checking on, then skip it (cause we don't want to perform a sum on itself
                if($i != $j)
                {
                    // Perform calculations both ways and see which one's absolute result is the smallest
                    $currentSum = min(abs($arrayValues[$i]-$arrayValues[$j]), abs($arrayValues[$j]-$arrayValues[$i]));

                    // If the sum result doesn't exist yet, add it to the container array
                    if(!in_array($currentSum, $sums))
                        $sums[] = $currentSum;
                }
            }
        }

        // Sort the container array in order to have the smallest absolute sum as the first element
        sort($sums);

        // Use this first element as the answer to the smallest absolute sum between all numbers in the array
        $sum = $sums[0];

        return $sum;
    }

    /**
     * function absoluteSum
     *
     * This function outputs all possible solutions to get to the sum of 100
     * by using a string of integers from 1 to 9
     *
     * <<DISCLAIMER>>, this one was quite tricky and I ended up taking inspiration from
     * a javascript solution done by a coder named Reginald
     *
     * @param array $arrayValues
     * @param int $total
     * @param String $sumString
     */
    function compute100($arrayValues=array(), $total=0, $sumString='')
    {
        // If there are values available we will process it
        if (count($arrayValues) != 0)
        {
            // Create a temporary array from the available values in order to manipulate it
            $tempArray = $arrayValues;

            // Get the first value in the current array
            $first = reset($tempArray);

            // Remove the first value from the array and grab the rest that are remaining
            array_shift($tempArray);
            $theRest = $tempArray;

            // Check if the sum string is not empty, if it is, it means it's the start of the string
            // in which case we only append the "first" value to it
            if ($sumString !== "")
            {
                // First we test by adding lone numbers
                $this->compute100($theRest, $total + $first, "{$sumString} + {$first}");

                // Secondly we test by subtracting lone numbers
                $this->compute100($theRest, $total - $first, "{$sumString} - {$first}");
            }
            else
                $this->compute100($theRest, $first, "{$first}");

            // We check if any more values are available to process
            if (count($theRest) > 0)
            {
                // Create a temporary array from the remaining values in order to manipulate it
                $tempArray2 = $theRest;

                // Get the first value in the remaining values array
                $second = reset($tempArray2);

                // Remove the first value from the remaining values array and grab the rest
                array_shift($tempArray2);
                $theRestB = $tempArray2;

                // We then pre-pend the first number multiplied by 10 with the second number added. This will give us a sequence of numbers
                // for instance to create a sequence of 123 we take 12(first and second number in array together) * 10 = 120 + 3(third number in array) = 123
                // (because 1, 2, 3 are the sequential numbers in the array)
                array_unshift($theRestB, $first*10+$second);

                // This last solution is used for testing concatenated numbers
                $this->compute100($theRestB, $total, $sumString);
            }
        }
        // If the total of all the values summed equal 100 then we want to print out the string
        elseif($total == 100)
        {
            echo $sumString.' = 100<br />';
        }
    }
}

$assessment = new Assessment();

/**
 * 1.
 * -------------------------------------------------------------------------------------
 * implement a function closestToZero to return the value closest to zero which is
 * supplied via the input array $arrayValues
 * - if $arrayValues is empty return 0
 * - if two numbers are equally close to zero, consider the positive number tobe closer to zero
 * -------------------------------------------------------------------------------------
 */
$input = array(7,-10,13,8,4,-7.2,-12,-3.7,3.5,-9.6,6.5,-1.7,-6.2,7);
echo "Result: " . $assessment->closestToZero($input) . "<br /><br />";

/**
 * 2.
 * -------------------------------------------------------------------------------------
 * Write three functions that compute the sum of the numbers in a given list using
 * a for-loop, a while-loop, and recursion.
 * -------------------------------------------------------------------------------------
 */
// For-Loop
$input = array(7,-10,13,8,4,-7.2,-12,-3.7,3.5,-9.6,6.5,-1.7,-6.2,7);
echo "Sum of numbers using a for-loop: " . $assessment->forLoop($input) . "<br /><br />";

// While-Loop
$input = array(7,-10,13,8,4,-7.2,-12,-3.7,3.5,-9.6,6.5,-1.7,-6.2,7);
echo "Sum of numbers using a while-loop: " . $assessment->whileLoop($input) . "<br /><br />";

// Recursive function
$input = array(7,-10,13,8,4,-7.2,-12,-3.7,3.5,-9.6,6.5,-1.7,-6.2,7);
echo "Sum of numbers using a recursive function: " . $assessment->recursiveFunction($input) . "<br /><br />";

/**
 * 3.
 * -------------------------------------------------------------------------------------
 * Write a function that combines two lists by alternatively taking elements. For
 * example: given the two lists [a, b, c] and [1, 2, 3], the function should return
 * [a, 1, b, 2, c, 3].
 * -------------------------------------------------------------------------------------
 */
$arrayA = array("a","b","c","d","e");
$arrayB = array(1, 2, 3, 4, 5, 6, 7, 8);
echo "Alternatively combined array output:";
echo "<pre>";
print_r($assessment->combineArrays($arrayA, $arrayB));
echo "</pre>";

// Using array_map to achieve the same result
echo "Alternatively combined array output using array_map function:";
echo "<pre>";
print_r($assessment->combineArraysUsingMap($arrayA, $arrayB));
echo "</pre>";

/**
 * 4.
 * -------------------------------------------------------------------------------------
 * Write a function that computes the list of the first 100 Fibonacci numbers.
 *
 * By definition, the first two numbers in the Fibonacci sequence are 0 and 1, and
 * each subsequent number is the sum of the previous two. As an example, here are
 * the first 10 Fibonnaci numbers: 0, 1, 1, 2, 3, 5, 8, 13, 21, and 34.
 * -------------------------------------------------------------------------------------
 */
$fibs = 100;
echo "Sum of the first {$fibs} Fibonacci numbers as above: " . $assessment->fibonacci($fibs) . "<br /><br />";

/**
 * 5.
 * -------------------------------------------------------------------------------------
 * Given array of integers, find the lowest absolute sum of elements.
 * -------------------------------------------------------------------------------------
 */
$input = array(7,-10,13,8,4,-7.2,-12,-3.7);
echo "<pre>";
print_r($input);
echo "</pre>";
echo "The lowest absolute sum of the above elements are: " . $assessment->absoluteSum($input) . "<br /><br />";

/**
 * 6.
 * -------------------------------------------------------------------------------------
 * Write a program that outputs all possibilities to put + or - or nothing between
 * the numbers 1, 2, ..., 9 (in this order) such that the result is always 100. For
 * example: 1 + 2 + 34 ? 5 + 67 ? 8 + 9 = 100.
 * -------------------------------------------------------------------------------------
 */
// I'm going to assume the question meant 1 to 9 as that seemed most obvious...
$arrayOfInts = array(1,2,3,4,5,6,7,8,9);
echo "The equations to get to 100 are as follows:<br />";
$assessment->compute100($arrayOfInts, 0, "");




use PHPUnit\Framework\TestCase;

class AssessmentTest extends TestCase
{
    public function test_closestToZero()
    {
        $assessment = new Assessment();
        $this->assertEquals(-1.7, $assessment->closestToZero(array(7,-10,13,8,4,-7.2,-12,-3.7,3.5,-9.6,6.5,-1.7,-6.2,7)));
    }

    public function test_forLoop()
    {
        $assessment = new Assessment();
        $this->assertEquals(-1.4, $assessment->forLoop(array(7,-10,13,8,4,-7.2,-12,-3.7,3.5,-9.6,6.5,-1.7,-6.2,7)));
    }

    public function test_whileLoop()
    {
        $assessment = new Assessment();
        $this->assertEquals(-1.4, $assessment->whileLoop(array(7,-10,13,8,4,-7.2,-12,-3.7,3.5,-9.6,6.5,-1.7,-6.2,7)));
    }

    public function test_recursiveFunction()
    {
        $assessment = new Assessment();
        $this->assertEquals(-1.4, $assessment->recursiveFunction(array(7,-10,13,8,4,-7.2,-12,-3.7,3.5,-9.6,6.5,-1.7,-6.2,7)));
    }

    public function test_combineArrays()
    {
        $assessment = new Assessment();

        $arrayA = array("a","b","c","d","e");
        $arrayB = array(1, 2, 3, 4, 5, 6, 7, 8);

        $this->assertEquals(array('a',1,'b',2,'c',3,'d',4,'e',5,6,7,8), $assessment->combineArrays($arrayA, $arrayB));
    }

    public function test_fibonacci()
    {
        $assessment = new Assessment();
        // PLEASE NOTE, the raw output of every number in the fibonacci 100 list is a bit much to assert for a test case,
        // but the return should be the below value
        $this->assertEquals(3.5422484817926, $assessment->fibonacci(100));
    }

    public function test_absoluteSum()
    {
        $assessment = new Assessment();
        $input = array(7,-10,13,8,4,-7.2,-12,-3.7);
        $this->assertEquals(1, $assessment->absoluteSum($input));
    }

    public function test_compute100()
    {
        $assessment = new Assessment();

        try {
            $arrayOfInts = array(1,2,3,4,5,6,7,8,9);
            $assessment->compute100($arrayOfInts);
        } catch (InvalidArgumentException $notExpected) {
            $this->fail();
        }

        $this->assertTrue(TRUE);
    }
}